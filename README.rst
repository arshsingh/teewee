===========
  teewee
===========

An app to keep track of your favourite TV Shows.
Live app: http://teewee.herokuapp.com

Installation
-------------
To run this project follow these steps:

 | $ git clone https://bitbucket.org/arshsingh/teewee.git
 | $ cd teewee

Assuming you have Python 2.7, VirtualEnvWrapper installed,
    
 | #. Create your working environment
 | $ mkvirtualenv teewee
 | $ workon teewee
 |
 | #. Install dependencies
 | $ pip install -r requirements/local.txt
 | 
 | #. Create SQLite database
 | $ python manage.py syncdb
 | $ python manage.py migrate
 |
 | #. Run the app
 | $ python manage.py runserver