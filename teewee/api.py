from django.contrib.auth.models import User
from django.conf.urls import url
from tastypie import fields
from tastypie.resources import Resource, ModelResource
from tastypie.authentication import SessionAuthentication
from tastypie.authorization import Authorization
from tastypie.bundle import Bundle
from tvdb_api import Show
from models import Subscription
import tvdb_utils


Show.__getattr__ = Show.__getitem__
Show.dict = None


class UserAuthorization(Authorization):
    def read_list(self, object_list, bundle):
        # This assumes a ``QuerySet`` from ``ModelResource``.
        return object_list.filter(user=bundle.request.user)

    def read_detail(self, object_list, bundle):
        # Is the requested object owned by the user?
        return bundle.obj.user == bundle.request.user

    def update_list(self, object_list, bundle):
        allowed = []

        # Since they may not all be saved, iterate over them.
        for obj in object_list:
            if obj.user == bundle.request.user:
                allowed.append(obj)

        return allowed

    def update_detail(self, object_list, bundle):
        return bundle.obj.user == bundle.request.user

    def delete_list(self, object_list, bundle):
        return object_list.filter(user=bundle.request.user)

    def delete_detail(self, object_list, bundle):
        return bundle.obj.user == bundle.request.user


class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'users'
        fields = ['id', 'username', 'first_name', 'last_name']
        allowed_methods = ['get']
        authentication = SessionAuthentication()
        authorization = Authorization()
        authorization.read_detail = lambda o, b: b.obj.id == b.request.user.id

    def obj_get(self, bundle, **kwargs):
        if not kwargs.get('username'):
            kwargs['username'] = bundle.request.user.username
        return super(UserResource, self).obj_get(bundle, **kwargs)

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}
        if isinstance(bundle_or_obj, Bundle):
            kwargs['username'] = bundle_or_obj.obj.username
        else:
            kwargs['username'] = bundle_or_obj.username
        return kwargs

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" %
                self._meta.resource_name, self.wrap_view('dispatch_detail'),
                name="api_dispatch_detail"),
            url(r"^(?P<resource_name>%s)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]


class SubscriptionResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user')

    class Meta:
        queryset = Subscription.objects.all()
        resource_name = 'subscriptions'
        authentication = SessionAuthentication()
        authorization = UserAuthorization()
        always_return_data = True

    def alter_deserialized_list_data(self, request, data):
        for i in range(len(data)):
            data[i]['user'] = request.user
        return data

    def alter_deserialized_detail_data(self, request, data):
        data['user'] = request.user
        return data


class TvdbResource(Resource):
    id = fields.IntegerField(attribute='id')
    seasons = fields.DictField(attribute='dict')
    data = fields.DictField(attribute='data')

    class Meta:
        object_class = object
        resource_name = 'shows'
        allowed_methods = ['get']
        authentication = SessionAuthentication()

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.id
        else:
            kwargs['pk'] = bundle_or_obj.id

        return kwargs

    def obj_get(self, bundle, **kwargs):
        show = tvdb_utils.get_show_by_id(int(kwargs['pk']))
        show.dict = self.build_tvdb_dict(show)
        return show

    def obj_get_list(self, bundle, **kwargs):
        if bundle.request.GET.get('q'):
            res = tvdb_utils.search_tvdb(bundle.request.GET.get('q'))
        else:
            res = []
            subs = Subscription.objects.filter(user=bundle.request.user)
            for sub in subs:
                res.append(tvdb_utils.get_show_by_id(sub.show_id))

        for each in res:
                each.dict = self.build_tvdb_dict(each)
        return res

    def build_tvdb_dict(self, show):
        result = dict(show)
        for i in result.keys():
            result[i] = dict(result[i])
            for n in result[i].keys():
                result[i][n] = dict(result[i][n])
        return result
