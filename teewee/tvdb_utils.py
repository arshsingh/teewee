from tvdb_api import Tvdb
from tvdb_ui import BaseUI
from tvdb_exceptions import tvdb_shownotfound


class CustomUI(BaseUI):

    def selectSeries(self, shows):
        ids = []
        for show in shows:
            ids.append(show['id'])
        return ids


def get_show_by_id(show_id):
    t = Tvdb()
    t.config['custom_ui'] = None
    try:
        show = t[int(show_id)]
    except:
        raise Exception('Incorrect ID')

    if 0 in show.keys():
        del show[0]

    return show


def search_tvdb(query):
    t = Tvdb()
    t.config['custom_ui'] = CustomUI
    try:
        ids = t._getSeries(query)
    except tvdb_shownotfound:
        return []

    results = []
    t.config['custom_ui'] = None
    for show in ids:
        results.append(get_show_by_id(show))
    results = list(reversed(sorted(results,
                                   key=lambda k: int(k['ratingcount']))))
    return results
