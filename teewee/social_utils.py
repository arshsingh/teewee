def set_avatar_url(request, backend, response, *args, **kwargs):
    """Pipeline to get user avatar from Twitter/FB via django-social-auth"""
    avatar_url = ''
    if backend.name == 'facebook':
        avatar_url = 'http://graph.facebook.com/%s/picture?type=small' \
                     % response['id']
    elif backend.name == 'twitter':
        avatar_url = response.get('profile_image_url', '')
    request.session['avatar_url'] = avatar_url
