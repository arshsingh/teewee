'use strict';

/* Filters */

angular.module('App.filters', [])

  .filter('prettifyDate', function() {
    return function(str) {
      if(str) {
        var date = str.split('-');
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return (date[2] + ' ' +
                months[parseInt(date[1], 10) - 1] + ' ' +
                date[0]);
      } else {
        return '';
      }
    }
  });