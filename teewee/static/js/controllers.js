'use strict';

/* Controllers */

angular.module('App.controllers', [])

  .controller('ShowListController', ['$rootScope', '$scope', 'Subscription', 'Show',
    function($rootScope, $scope, Subscription, Show) {
      $rootScope.title = 'Dashboard';

      Show.getAll().then(function(data){
        $scope.shows = _.sortBy(data, function(s){
                                  return s.nextEpisode() ?
                                  s.nextEpisode().firstaired || 'x' :
                                  'y';
                                });
      });
      
      Subscription.getAll().then(function(data){
        $scope.subscriptions = [];
        _.each(data, function(s){ $scope.subscriptions[s.show_id] = s; });
      });
    }
  ])

  .controller('ShowController', ['$rootScope', '$scope', '$routeParams',
                                 'Subscription', 'Show',
    function($rootScope, $scope, $routeParams, Subscription, Show) {
      Show.get($routeParams.id).then(function(data){
        $scope.show = data;
        $rootScope.title = data.data.seriesname;
      });

      Subscription.getAll().then(function(data){
        $scope.subscription = _.find(data, function(s){
                                      return s.show_id == $routeParams.id;
                                    });
        if(!$scope.subscription) { $scope.showDetails = true; }
      });

      $scope.markLastSeen = function(ep){
        var oldLastSeen = $scope.subscription.last_seen;
        $scope.subscription.last_seen = ep.seasonnumber + '-' + ep.episodenumber;
        $scope.subscription.save().then(function(status) {
          // revert if update unsuccessfull
          if(status > 202){
            $scope.subscription.last_seen = oldLastSeen;
          }
        });
      };

      $scope.subscribe = function() {
        var sub = new Subscription();
        sub.show_id = $scope.show.id;
        sub.last_seen = '1-0';
        sub.save().then(function(data) {
          $scope.subscription = data;
        });
      };

      $scope.unsubscribe = function() {
        $scope.subscription.delete();
        $scope.subscription = undefined;
      };

      $scope.toArray = _.values;
    }
  ])

  .controller('SearchController', ['$scope', '$location', 'Show', 'User',
    function($scope, $location, Show, User){
      $scope.search = function() {
        $scope.loading = true;
        if($scope.query) {
          Show.search($scope.query).then(function(data){
            $scope.loading = false;
            if(data.length==1) {
              $scope.selectShow(data[0].id);
            } else {
              $scope.results = data;
            };
  
          });
        }
      };

      $scope.selectShow = function(id){
        $scope.results = undefined;
        $scope.query = undefined;
        $location.path('/show/' + id);
      }

      User.get().then(function(data){
        $scope.user = data;
      })
    }
  ]);