'use strict';

angular.module('App', ['App.models', 'App.controllers', 'App.filters', 'LocalStorageModule'])
  
  .constant('API_ROOT', '/api/v1')
  .constant('STATIC_URL', '/static')
  
  .config(['$routeProvider', '$httpProvider', '$locationProvider',
           '$interpolateProvider', 'STATIC_URL',
    function($routeProvider, $httpProvider, $locationProvider,
             $interpolateProvider, STATIC_URL) {
        
        $locationProvider.html5Mode(true);
        
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $routeProvider.when('/', {
            templateUrl: STATIC_URL + '/views/dash.html'
        });
        $routeProvider.when('/show/:id', {
            templateUrl: STATIC_URL + '/views/show.html',
            controller: 'ShowController'
        });
    }
  ]);