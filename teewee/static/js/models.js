'use strict';

/* Models */

angular.module('App.models', [])

  .factory('User', ['$http', 'API_ROOT',
    function($http, API_ROOT) {
      var User = function(data) {
        angular.extend(this, data);
      };

      User.get = function(username){
        var url = API_ROOT + '/users/' + (username ? username + '/' : '');
        return $http.get(url).then(function(response){
          return new User(response.data);
        });
      };

      User.prototype.displayName = function() {
        return (this.first_name ? this.first_name + ' ' : '') +
               (this.last_name || '') ||
               this.username;
      };

      return User;
    }
  ])
  
  .factory('Subscription', ['$http', '$q','$cacheFactory', 'API_ROOT', 'Show',
    function($http, $q, $cacheFactory, API_ROOT, Show) {
      var Subscription = function(data) {
        angular.extend(this, data);
      };
      var cache = $cacheFactory('subscriptions');

      Subscription.get = function(id){
        var subs = cache.get('subs');
        if(subs && subs.hasOwnProperty(id)){
          var deferred = $q.defer();
          deferred.resolve(new Subscription(subs[id]));
          return deferred.promise;
        }
        else {
          var url = API_ROOT + '/subscriptions/' + id + '/';
          return $http.get(url, {cache: false}).then(function(response){
            (subs || {})[id] = response.data;
            cache.put('subs', subs);
            return new Subscription(response.data);
          });
        }
      };

      Subscription.getAll = function(){
        if(cache.get('subs')){
          var deferred = $q.defer();
          deferred.resolve(_.map(cache.get('subs'), function(s) {
            return new Subscription(s);
          }));
          return deferred.promise;
        }
        else {
          var url = API_ROOT + '/subscriptions/';
          return $http.get(url, {cache: false}).then(function(response){
            var subs = {};
            var result = _.map(response.data.objects, function(s){
              subs[s.id] = s;
              return new Subscription(s);
            });
            cache.put('subs', subs);
            return result;
          });
        }
      };

      Subscription.prototype.save = function() {
        var self = this;
        var url = API_ROOT + '/subscriptions/';
        var subs = cache.get('subs');
        
        if(self.id){
          url = url + self.id + '/';
          return $http.put(url, self).then(function(response){
            if(response.status < 205){
              subs[self.id] = response.data;
              cache.put('subs', subs);
            }
            return response.status;
          });
        } else {
          return $http.post(url, self).then(function(response){
            if(response.status < 205){
              subs[response.data.id] = response.data;
              cache.put('subs', subs);
            }
            return new Subscription(response.data);
          });
        }
      };

      Subscription.prototype.delete = function() {
        var self = this;
        var url = API_ROOT + '/subscriptions/' + self.id + '/';

        return $http.delete(url).then(function(response){
          if(response.status < 205){
            var subs = cache.get('subs');
            delete subs[self.id];
            cache.put('subs', subs);
          }
          return response.status;
        });
      };

      return Subscription;
    }
  ])

  .factory('Show', ['$http', '$q', 'API_ROOT', 'localStorageService',
    function($http, $q, API_ROOT, localStorageService) {
      var Show = function(data) {
        angular.extend(this, data);
      };

      Show.get = function(id){
        if(localStorageService.keys().indexOf('show-' + id) >= 0){
          var deferred = $q.defer();
          deferred.resolve(new Show(localStorageService.get('show-' + id)));
          return deferred.promise;
        }
        else {
          var url = API_ROOT + '/shows/' + id + '/';
          return $http.get(url, {cache: false}).then(function(response){
            localStorageService.set('show-' + id, response.data);
            return new Show(response.data);
          });
        }
      };

      Show.getAll = function(){
        var cacheKeys = _.filter(localStorageService.keys(), function(key){
          return key.slice(0,5) == 'show-';
        });
        if(cacheKeys.length > 0){
          var deferred = $q.defer();
          deferred.resolve(_.map(cacheKeys, function(key){
            return new Show(localStorageService.get(key));
          }));
          return deferred.promise;
        }
        else {
          var url = API_ROOT + '/shows/';
          return $http.get(url, {cache: false}).then(function(response){
            return _.map(response.data.objects, function(s){
              localStorageService.set('show-' + s.id, s);
              return new Show(s);
            });
          });
        }
      };

      Show.search = function(q){
        var url = API_ROOT + '/shows/';
        return $http.get(url, {params: {q: q}, cache: true}).then(function(response){
          return _.map(response.data.objects,
                       function(s){ return new Show(s); });
        });
      };

      Show.prototype.unseen = function(lastSeen){
        var show = this;
        return _.chain(show.seasons)
                .values()
                .map(function(s){ return _.values(s); })
                .flatten()
                .filter(function(e){ return show.isAired(e) && !show.isSeen(lastSeen, e); })
                .value();
      };

      Show.prototype.nextEpisode = function() {
        var show = this;
        var ep = _.chain(show.seasons)
                  .values()
                  .map(function(s){ return _.values(s); })
                  .flatten()
                  .filter(function(e){ return !show.isAired(e); })
                  .first()
                  .value();

        this.nextEpisode = function(){ return ep; };
        return ep;
      };

      Show.prototype.isSeen = function(lastSeen, ep){
        var last = lastSeen.split('-');
        return ep.seasonnumber == parseInt(last[0], 10) ?
               ep.episodenumber <= parseInt(last[1], 10) :
               ep.seasonnumber < parseInt(last[0], 10);
      };

      Show.prototype.isAired = function(ep){
        return ep.firstaired ?
               (new Date()).toISOString().slice(0,10) >= ep.firstaired :
               false
      };

      return Show;
    }
  ]);