from django.shortcuts import render, redirect
from django.contrib.auth import logout as signout
from django.views.decorators.csrf import ensure_csrf_cookie


def logout(request):
    signout(request)
    return redirect('home')


@ensure_csrf_cookie
def home(request):
    if request.user.is_authenticated():
        return render(request, 'app.html')
    else:
        return render(request, 'index.html')
