from django.db import models
from django.contrib.auth.models import User


class Subscription(models.Model):
    user = models.ForeignKey(User, related_name='subscription')
    show_id = models.IntegerField(null=False)
    last_seen = models.CharField(max_length=10, default="0-0")

    def __unicode__(self):
        return str(self.user) + str(self.show_id)
