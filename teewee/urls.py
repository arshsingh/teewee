from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from tastypie.api import Api
from api import UserResource, SubscriptionResource, TvdbResource
from models import Subscription
import views

admin.autodiscover()
admin.site.register(Subscription)

api = Api(api_name='v1')
api.register(UserResource())
api.register(SubscriptionResource())
api.register(TvdbResource())

urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'', include('social_auth.urls')),
    url(r'^api/', include(api.urls)),
    url(r'^logout/', views.logout),

    # Examples:
    # url(r'^$', 'teewee.views.home', name='home'),
    # url(r'^teewee/', include('teewee.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

if not settings.DEBUG:
    urlpatterns += patterns('', (r'^static/(?P<path>.*)$',
                            'django.views.static.serve',
                            {'document_root': settings.STATIC_ROOT}),
                            )
